class data_type2{
	public static void main(String []args){
//--------------------------------------------------------------------------------------------------------
		//character
		 char wing1=97;
                System.out.println(wing1);//print output:a
		char wing2=156;
		System.out.println("char 156="+wing2);//print output:(as a blank)
		
		System.out.println(Character.MAX_VALUE);//output:65535
		System.out.println(Character.SIZE);//output:16
		//char wing3=65536;//incomatible types:possibly loosely conversion from int to char
	        //System.out.println(wing3);
//------------------------------------------------------------------------------------------------------
		//boolean
		boolean single=true;
		System.out.println(single);
//		single=1;         incompatible types:int canot assigned to boolean
		System.out.println(single);
//------------------------------------------------------------------------------------------------------

	}

}
