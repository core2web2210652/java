class Data_types{
	public static void main(String []args){
		int val=123.0;//incompatible types:possibly loosely conversion from double to int.
		int value=123;//correct one
		float CGPA=8.0;//incompatible types:possibly loosely conversion from double to float
		char wing='S';
		System.out.println(CGPA);
                System.out.println(wing);
		System.out.println(Integer.SIZE);//find size of data types
		System.out.println(Integer.MAX_VALUE);//find maximum range of data type
		System.out.println(Integer.MIN_VALUE);//find minimum range of data type
//--------------------------------------------------------------------------------------------------------------------
		char wing1=97;
		System.out.println(wing1);

	}
}
