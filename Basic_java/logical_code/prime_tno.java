import java.util.*;
class Prime{
	public static void main(String []args){
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a starting point of range");
		int start=sc.nextInt();
		System.out.println("Enter a ending point of range");
		int end=sc.nextInt();
		for(int i=start;i<=end;i++){
			int j=1;
			int count=0;
			while(j<=i){
				if(i%j==0){
					count++;
				}
				j++;
			}
			if(count==2){
				System.out.print(i+"\t");
			}
		
		}
	}
}
