/*
 * 10. WAP to print the Maximum element in the array.
Example:
Input:
Enter the size
5
Enter elements:
7
81
65
12
23
Output:
Maximum number in the array is found at pos 1 is 81
 */
import java.util.*;
class Program10{
            public static void main(String []args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter size of an array");
                int size=sc.nextInt();
		int index=0;
               
		int []arr1=new int[size];
		 int max=arr1[0];
                 for(int i=0;i<arr1.length;i++){
                        System.out.print("enter number:-\t");
                        arr1[i]=sc.nextInt();
                }
		for(int i=0;i<arr1.length;i++){
			if(arr1[i]>max){
				max=arr1[i];
				index=i;
			}
		}
		System.out.println("Maximum number in the array is found at pos "+index+" is "+max);

            }
}
