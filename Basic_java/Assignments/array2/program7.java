/*
 * 7.WAP to print the array , if the user given size of an array is even then print the alternate
elements in an array, else print the whole array.
Example 1:
Input:
Enter the size
5
Enter elements:
1
2
3
4

5
Output:
 */
 import java.util.*;
class Program7{ 
            public static void main(String []args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter size of an array");
                int size=sc.nextInt();
                int []arr1=new int[size];
                 for(int i=0;i<arr1.length;i++){
                        System.out.print("enter number:-\t");
                        arr1[i]=sc.nextInt();
                }
	       if(size%2==0){
		for(int i=0;i<arr1.length;i+=2){
	       		System.out.print(" "+arr1[i]);	
	       }	
	       }
	       else{
	       	System.out.println("");
	       }
				
            }                                                        
}
