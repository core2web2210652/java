/*
 * 9. WAP to print the minimum element in the array, where you have to take the size and
elements from the user.
Example:
Input:
Enter the size
5
Enter elements:
5
6
9
-9
17
Output:

Minimum number in the array is : -9
*/
import java.util.*;
class Program9{
            public static void main(String []args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter size of an array");
                int size=sc.nextInt();
                int []arr1=new int[size];
		int min=arr1[0];
                 for(int i=0;i<arr1.length;i++){
                        System.out.print("enter number:-\t");
                        arr1[i]=sc.nextInt();
                }
		for(int i=0;i<arr1.length;i++){
			if(arr1[i]<min){
				min=arr1[i];
			}
		}
		System.out.println("minimum element is:-"+min);
            }
}
