/*
 * 2.WAP to print the sum of elements divisible by 3 in the array, where you have to take the
size and elements from the user.
Example:
Enter size : 8
9 13 5 13 6 22 36 10
output:
Elements divisible by 3 : 9 6 36
Sum of elements divisible by 3 is: 51
*/
import java.util.*;
class Program2{
            public static void main(String []args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter size of an array");
                int size=sc.nextInt();
                int []arr1=new int[size];
		int count =0;
                 for(int i=0;i<arr1.length;i++){
                        System.out.print("enter number:-\t");
                        arr1[i]=sc.nextInt();
                }
		System.out.print("elements divisible by 3:");
		for(int i=0;i<arr1.length;i++){
			if(arr1[i]%3==0){
				count++;
				System.out.print(" "+arr1[i]);
			}
		}
		System.out.println("\n Sum of elements divisible by 3 is:"+count);
            }
}
