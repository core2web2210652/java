/*
 * 5.Write a program to print the sum of odd indexed elements, in an array. Where you have
to take size input and elements input from the user .
Example:
Input:
Enter the size
5
Enter elements:
4
8
2
6
7
Output:
Sum of odd indexed elements : 14
*/
import java.util.*;
class Program5{
            public static void main(String []args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter size of an array");
                int size=sc.nextInt();
		int sum=0;
                int []arr1=new int[size];
                 for(int i=0;i<arr1.length;i++){
                        System.out.print("enter number:-\t");
                        arr1[i]=sc.nextInt();
                }
		for(int i=0;i<arr1.length;i++){
			if(i%2==1){
				sum+=arr1[i];
			}
		}
		System.out.println("sum of odd indexed element:- "+sum);}
            
}
