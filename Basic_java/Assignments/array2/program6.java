/*
 *6.Write a program to print the products of odd indexed elements in an array. Where you
have to take size input and elements input from the user.
Note:
Example:
Input:
Enter the size
6
Enter elements:
1
2
3
4
5
6
Output:
product of odd indexed elements : 48
 */
import java.util.*;
class Program6{
            public static void main(String []args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter size of an array");
                int size=sc.nextInt();
                int product=1;
                int []arr1=new int[size];
                 for(int i=0;i<arr1.length;i++){
                        System.out.print("enter number:-\t");
                        arr1[i]=sc.nextInt();
                }
                for(int i=0;i<arr1.length;i++){
                        if(i%2==1){
                                product*=arr1[i];
                        }
                }
                System.out.println("product of odd indexed element:- "+product);}

}
