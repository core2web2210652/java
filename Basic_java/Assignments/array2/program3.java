/*
 * 3.WAP to check if there is any vowel in the array of characters if present then print its
index, where you have to take the size and elements from the user.
Example:
Input:
Enter the size
5
Enter elements:
arEKO
Output:
vowel a found at index 0
vowel E found at index 2
vowel O found at index 4
 */
import java.util.*;
class Program3{
            public static void main(String []args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter size of an array");
                int size=sc.nextInt();
                char []arr1=new char[size];
                 for(int i=0;i<arr1.length;i++){
                        System.out.print("enter character:-\t");
                        arr1[i]=sc.next().charAt(0);
                
		 }
		 char []arr2=Arrays.copyOf(arr1,arr1.length);
		for(int i=0;i<arr1.length;i++){
			
				arr1[i]=Character.toLowerCase(arr1[i]);
				

				if(arr1[i]=='a'||arr1[i]=='e'||arr1[i]=='i'||arr1[i]=='o'||arr1[i]=='u'){
					System.out.print("vowel "+arr2[i]+" fount at index "+i);
					System.out.println("");

				}
			
			
		}
            }
}
