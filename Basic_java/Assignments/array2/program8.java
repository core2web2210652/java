/*
 *
 * 3
8.WAP to print the elements in an array which are greater than 5 but less than 9, where
you have to take the size and elements from the user.
Example:
Input:
Enter the size
5
Enter elements:
4
8
6
9
41
Output:
8 is greater than 5 but less than 9
6 is greater than 5 but less than 9
 */
import java.util.*;
class Program8{
            public static void main(String []args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter size of an array");
                int size=sc.nextInt();
                int []arr1=new int[size];
                 for(int i=0;i<arr1.length;i++){
                        System.out.print("enter number:-\t");
                        arr1[i]=sc.nextInt();
                }
		for(int i=0;i<arr1.length;i++){
			if(arr1[i]<9 && arr1[i]>5){
				System.out.println(arr1[i]+" is greater than 5 but less than 9");
			}
		}
            }
}
