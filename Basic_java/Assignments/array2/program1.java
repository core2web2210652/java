/*
 * 1.WAP to count the even numbers in an array where you have to take the size and
elements from the user. And also print the even numbers too
Example:
Enter size =8
1 12 55 65 44 22 36 10
Output : even numbers 12 44 22 36 10
Count of even elements is : 5
*/
import java.util.*;
class Program1{
            public static void main(String []args){
                Scanner sc=new Scanner(System.in);
                int count=0;
                System.out.println("enter size of an array");
                int size=sc.nextInt();
                int []arr1=new int[size];
                 for(int i=0;i<arr1.length;i++){
                        System.out.print("enter number:-\t");
                        arr1[i]=sc.nextInt();
                }
		System.out.print("even numbers are ");
                for(int i=0;i<arr1.length;i++){
                        if(arr1[i]%2==0){
                                System.out.print(" "+arr1[i]);
                                count++;
                        }

                }
                System.out.println("\ncount of even element is:-"+count);
            }
}
