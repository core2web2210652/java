/*
 * 5. Write a program where you have to print the elements from the array which are less
than 10.Take input from the user.
Example:
Enter size: 10

11 2 18 9 10 5 16 20 8 10
Output :
2 is less than 10
9 is less than 10
5 is less than 10
8 is less than 10
*/
import java.util.*;
class Program5{
            public static void main(String []args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter size of an array");
                int size=sc.nextInt();
                int []arr1=new int[size];
                 for(int i=0;i<arr1.length;i++){
                        System.out.print("enter number:-\t");
                        arr1[i]=sc.nextInt();
                }
		for(int i=0;i<arr1.length;i++){
			if(arr1[i]<10){
				System.out.println(arr1[i]+" is less than 10");
			}
		}
            }
}
