/*
 * 1. Write a program to print the array with minimum 10 elements data.
Example:
Array:
10 20 30 40 50 60 70 80 90 100
Output :
10, 20, 30, 40, 50, 60, 70, 80, 90, 100
*/
import java.util.*;
class Program1{
	public static void main(String []args){
		
		int arr1[]=new int[]{10,20,30,40,50,60,70,80,90,100};
	
		for(int i=0;i<arr1.length;i++){
			System.out.print(arr1[i]+"\t");
			if(i<arr1.length-1){
				System.out.print(",");
			}
		}
	}
}
