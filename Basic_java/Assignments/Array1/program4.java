
/*
 * 4. Write a program to print the sum of odd elements in an array.Take input from the user.
Example:
Enter size: 10
Array:
1 2 3 4 2 5 6 2 8 10
Output :
Sum of odd elements : 9
 */
import java.util.*;
class Program4{
            public static void main(String []args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter size of an array");
                int size=sc.nextInt();
		int sum=0;
                int []arr1=new int[size];
		 for(int i=0;i<arr1.length;i++){
                        System.out.print("enter number:-\t");
                        arr1[i]=sc.nextInt();
                }
		for(int i=0;i<arr1.length;i++){
			if(arr1[i]%2==1){
				sum=sum+arr1[i];
			}
		}
		System.out.print("sum of odd elemnts:- "+sum);
            }
}
