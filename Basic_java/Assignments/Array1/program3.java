/*
 * 3. Write a program to print the even elements in the array. Take input from the user.
Example :
Enter size : 10
Array:
10 11 12 13 14 15 16 17 18 19
Output :
10
12
14
16
18*/
import java.util.*;
class Program3{
	public static void main(String []args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter size of an array");
		int size=sc.nextInt();
		int []arr1=new int[size];
		for(int i=0;i<arr1.length;i++){
			System.out.print("enter number:-\t");
			arr1[i]=sc.nextInt();
		}
		for(int i=0;i<arr1.length;i++){
			if(arr1[i]%2==0){
				System.out.println(arr1[i]);
			}
		}
	}
}
