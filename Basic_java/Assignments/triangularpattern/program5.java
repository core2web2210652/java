/*
 *4. WAP in notebook & Dry run first then type
Take number of rows from user :
Row = 3
3
3 6
3 6 9*
 * */

import java.util.*;
class Program5{
        public static void main(String []args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter number of rows:-");
                int rows=sc.nextInt();

                for(int i=1;i<=rows;i++){
                        int a=3;
                        for(int j=0;j<i;j++){
                                System.out.print(a+" ");
                                a+=3;

                        }
                        System.out.println();
                }
        }
}
