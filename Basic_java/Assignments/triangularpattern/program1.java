/* 
 	1:-Take number of rows from user
Row=4

*
* *
* * *
* * * *

 
 */
import java.util.*;
class Program1{
	public static void main(String []args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter number of rows:-");
		int rows=sc.nextInt();
		for(int i=1;i<=rows;i++){
			for(int j=0;j<i;j++){
				System.out.print("* ");

			}
			System.out.println();
		}
	}
}
