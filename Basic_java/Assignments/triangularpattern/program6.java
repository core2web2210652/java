/*
 *5. WAP in notebook & Dry run first then type
Take number of rows from user :
Row = 3
1
2 4
3 6 9
 * */

import java.util.*;
class Program6{
        public static void main(String []args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter number of rows:-");
                int rows=sc.nextInt();
		int val;
                for(int i=1;i<=rows;i++){
                       val=i;
                        for(int j=0;j<i;j++){
                                System.out.print(val+" ");
                                val+=i;

                        }
                        System.out.println();
                }
        }
}
