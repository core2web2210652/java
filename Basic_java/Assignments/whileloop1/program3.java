/*Program 3:- Write a program to print the character sequence given below when the
range given is
Input : start = 1 and end =6.
This means iterate the loop from 1 to 6
Output: A B C D E F*/
class Program3{
	public static void main(String []args){
		int  start=1;
		int end=6;
		int  ch=65;//we can also write it like this:-[way1:char ch='A'] ,[way2:char ch=65]
		while(start<=end){
				
			System.out.print((char)ch+++"\t");
			start++;
		}
	}
}
