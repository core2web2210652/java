/*
 * Write a program to print each digit on a new line of a given number
using a while loop
Input: num = 9307
Output: 7
0
3
9
*/
import java.util.*;
class Program6{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter number:-");
		int num=sc.nextInt();
		int remainder;
		int add=0;

		while(num!=0){
			remainder=num%10;
			System.out.println(remainder);
			add=add*10+remainder;
			num/=10;

		}
		System.out.println(add);

	}
}
