/*Row= 4
1 3 5 7
9 11 13 15
17 19 21 23
25 27 29 31*/
class Program5{
	public static void main(String []args){
		int Row=4;
		int val=1;
		for(int i=1;i<=Row;i++){
			for(int j=1;j<=Row;j++){
				System.out.print(val+"\t");
				val+=2;
			}
			System.out.println();
		}
	}
}
