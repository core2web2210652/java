/*
 * 9.
 * Write a program to count the odd digits and even digits in the given
number.
Input: 214367689
Output: Odd count : 4
Even count : 5*/
import java.util.*;
class Program9{
	public static void main(String []args){
	
		Scanner sc=new Scanner(System.in);
		System.out.println("enetr a number");
		int num=sc.nextInt();
		int remainder=0;
		int count1=0;
		int count2=0;
		while(num!=0){
			remainder=num%10;
			if(remainder%2==0){
				count1++;
			}
			else{
				count2++;
			}
			num/=10;
		}
		System.out.println("odd number:-"+count2);
		System.out.println("even number:-"+count1);
	}
}
