/*
7.Write a program to count the digits in the given number.
Input: num = 93079224
Output: Count of digits= 8
*/
import java.util.*;
class Program7{
	public static void main(String []args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter a number");
		int num = sc.nextInt();
		int remainder=0;
		int count=0;
		while(num!=0){
			num/=10;
			count++;
		}
		System.out.println(count);
	}
}
