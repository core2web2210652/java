/*
 * 8. Write a program to print the odd digits of a given number.
Input : 216985
Output : 5 9 1

*/
import java.util.*;
class Program8{
	public static void main(String []args){
		int remainder=0;
		Scanner sc=new Scanner(System.in);
		System.out.println("enter a number");
		int num=sc.nextInt();
		int count=0;
		List<Integer>list=new ArrayList<Integer>();
		
		while(num!=0){
			
			remainder=num%10;
			if(remainder%2==1){
				 list.add(remainder);
				//System.out.println(remainder);
			}
			 
			num/=10;
		}
		  for(int i=list.size()-1;i>=0;i--){
                              System.out.println(list.get(i));
                        }
	}
}


