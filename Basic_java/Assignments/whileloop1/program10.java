/*
 * 10.Write a program to print the sum of digits in the given number.
Input: 9307922405
Output: sum of digits in 9307922405 is 41
*/
import java.util.*;
class Program10{
	public static void main(String []args){
		int sum=0;
		int remainder;
		Scanner sc=new Scanner(System.in);
		System.out.println("enter number");
		int num=sc.nextInt();
		while(num!=0){
			remainder=num%10;
			sum+=remainder;
			num/=10;
		}
		System.out.println("sum:-"+sum);
	}
}
