/*
 * Q5 Write a program to print the factorial of the number.
Input : 5
Output : Factorial of 5 is 120.
 */
import java.util.*;
import java.io.*;
class Program5{
	public static void main(String []args){
		Scanner sc=new Scanner(System.in);
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("enter a number");
		int num=sc.nextInt();
		int product=1;
		// int num=Integer.parseInt(br.readLine());
		for(int i=1;i<=num;i++){
			product*=i;
		}
		 System.out.println("factorial of "+num+" is "+product);
	}
	
}
