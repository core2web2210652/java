/*
 * Q8 Write a program to reverse the given number.
Input : 7853
Output : Reverse of 7853 is 3587.
 */
import java.util.*;
import java.io.*;
class Program8{
        public static void main(String []args)throws IOException{
                Scanner sc=new Scanner(System.in);
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("enter a number");
                //int num=sc.nextInt();
                int num=Integer.parseInt(br.readLine());
                int quotient=num;
                int rem=0,sum=0;
                while(quotient!=0){
                        rem=quotient%10;
                        sum=sum*10+rem;
                        quotient/=10;
                }
                System.out.println("reverse of "+num+" is "+sum);
        }
}
