/*
 * Q9 Write a program to check whether the given number is palindrome or
not.
Input : 12121
Output : 12121 is a palindrome number.
Input : 12345
Output : 12345 is not a palindrome number.
 */
import java.util.*;
import java.io.*;
class Program9{
        public static void main(String []args)throws IOException{
                Scanner sc=new Scanner(System.in);
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("enter a number");
                //int num=sc.nextInt();
                int num=Integer.parseInt(br.readLine());
                int quotient=num;
                int rem=0,sum=0;
                while(quotient!=0){
                        rem=quotient%10;
                        sum=sum*10+rem;
                        quotient/=10;
                }
                if(num==sum){
			System.out.println("number is pallindrome");
		}else{
			 System.out.println("number is not pallindrome");
		}
        }
}
