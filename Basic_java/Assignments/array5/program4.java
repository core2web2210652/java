/*
 *Q4. WAP to check the first duplicate element in an array and return its index.
Example :
Input:
Enter the size of the array:
6
Enter the elements of the array:
1
2
3
3
2
5
Output:
First duplicate element present at index 1
 */
import java.util.*;
class Program4{
        public static void main(String[]args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter size:");
                int size=sc.nextInt();
                int arr[]=new int[size];
		boolean flag=false;
                
                for(int i=0;i<arr.length;i++){
                        System.out.println("enter an element:-");
                        arr[i]=sc.nextInt();

                 }
		int index=0;
                for(int i=0;i<arr.length;i++){
			for(int j=i+1;j<arr.length;j++){
				//if(j!=i){
					if(arr[i]==arr[j]){
						index=i;
						flag=true;
						break;
					}
				//}
			}
			if(flag){
				break;
			}
			
                }
		System.out.println("first dupilcate element is present at index "+index);
        }
}
