/*
 * Q3. WAP to check if an array is a palindrome or not .
Example :
Input:
Enter the size of the array:
6
Enter the elements of the array:
1
2
3
3
2
1
Output:
The given array is a palindrome array.
 */
import java.util.*;
class Program3{
        public static void main(String[]args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter size:");
                int size=sc.nextInt();
                int arr[]=new int[size];
                
                for(int i=0;i<arr.length;i++){
                        System.out.println("enter an element:-");
                        arr[i]=sc.nextInt();

                 }
		boolean flag=false;
                for(int i=0;i<arr.length;i++){
			if(arr[i]==arr[size-i-1]){
				flag=true;
			}else{
				flag=false;
			}

                }
		if(flag){
			System.out.println("given array is a pallindrome");
		}else{
			System.out.println("given array is not pallindrome");
		}
        }
}
