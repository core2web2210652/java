/*
 * Q9. WAP to take a number from the user and store each element in an array by increasing
the element by one.
Example:
Input:
Enter the Number:
1569872365
Output:
2, 6, 7, 10, 9, 8, 3, 4, 7, 6
Explanation : Each digit in a number is increased by one and stored in an array
 */
import java.util.*;
class Program9{
        public static void main(String[]args){
                Scanner sc=new Scanner(System.in);
                int count=0,rem=0;
                int num=sc.nextInt();
		int number=num;
               while(num!=0){
	       	rem=num%10;
		count++;
		num=num/10;
		
	       }
	      System.out.println("count:-"+count);
	       int arr[]=new int[count];
	      count=0;
	      while(number!=0){                                                                                                                         rem=number%10;
                arr[count]=rem+1;
		
		count++;
                number=number/10;


               }
	      for(int i=arr.length-1;i>=0;i--){
	      	System.out.print(arr[i]+" ,");
	      }

               
		
        }
}
