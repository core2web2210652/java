/*
 * Q1. WAP to check whether the array is in ascending order or not.
Input 1:
Enter the size of the array:
4
Enter the elements of the array:
1
5
9
15
Output 1:
The given array is in ascending Order.
Input 2 :
Enter the size of the array:
4
Enter the elements of the array:
1
5
9
7
Output 2 :
The given array is not in ascending Order.

 */
import java.util.*;
class Program1{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter size:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		int flag=0;
		for(int i=0;i<arr.length;i++){
			System.out.println("enyter an element:-");
			arr[i]=sc.nextInt();
		}
		for(int i=0;i<arr.length-1;i++){
			if(arr[i]>arr[i+1]){
				System.out.println("array is not in ascending order");
				break;
			}else{
				flag=1;
			}
		}
		if(flag==1){
			System.out.println("array in ascending order");
		}
	}


}
