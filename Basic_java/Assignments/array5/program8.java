/*
 *Q8. WAP to find the second minimum element in an array.
Example:
Input:
Enter the size:5
Enter the elements of the array:
10
2
31

4
0
Output:
The second largest element in the array is: 2
 */
import java.util.*;
class Program8{
        public static void main(String[]args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter size:");
                int size=sc.nextInt();
                int arr[]=new int[size];
                int flag=0;
                for(int i=0;i<arr.length;i++){
                        System.out.println("enter an element:-");
                        arr[i]=sc.nextInt();

                 }
		int min=arr[0];
                for(int i=0;i<arr.length;i++){
			if(min>arr[i]){
				min=arr[i];
			}

                }
		int min2=99999999;
		for(int i=0;i<arr.length;i++){
			if(min!=arr[i]){
				if(min2>arr[i]){
					min2=arr[i];
				}
			}
		}
		System.out.println("second largest element:-"+min2);
        }
}
