/*
 * Q5. WAP to print the count of digits in elements of an array.
Example :
Input:
Enter the size of the array:
4
Enter the elements of the array:
1
225
32
356

Output:
1, 3, 2, 3
 */
import java.util.*;
class Program5{
        public static void main(String[]args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter size:");
                int size=sc.nextInt();
                int arr[]=new int[size];
                int remainder=0;
                for(int i=0;i<arr.length;i++){
                        System.out.println("enter an element:-");
                        arr[i]=sc.nextInt();

                 }
		String []arrays=new String[size];
                for(int i=0;i<arr.length;i++){
			int count=0;
			while(arr[i]!=0){
				remainder=arr[i]%10;
				count++;
				arr[i]/=10;
			}
			arrays[i]=Integer.toString(count);

                }
		String s=String.join(",",arrays);
		System.out.println(s);
        }
}
