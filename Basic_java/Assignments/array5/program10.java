/*
 *Q10. WAP to print the factorial of each element in an array.
Example :
Input:
Enter the size of the array:
6
Enter the elements of the array:
1
2
3
5
8
2

Output:
1, 2, 6, 120, 40320, 2
 */
import java.util.*;
class Program10{
        public static void main(String[]args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter size:");
                int size=sc.nextInt();
                int arr[]=new int[size];
                int flag=0;
                for(int i=0;i<arr.length;i++){
                        System.out.println("enter an element:-");
                        arr[i]=sc.nextInt();

                 }
                for(int i=0;i<arr.length;i++){
			int product=1;
			for(int j=1;j<=arr[i];j++){
				product*=j;
			}
			System.out.print(product+" , ");

                }
        }
}
