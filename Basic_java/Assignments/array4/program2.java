/*
 * Q2.WAP to find the difference between minimum element in an array and maximum
element in an array, take input from the user.
Example :
Input:
Enter the size :
5
Enter the elements of the array:
3
6
9
8
10
Output
The difference between the minimum and maximum elements is: 7
*/
import java.util.*;
class Program2{
	public static void main(String []args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter size of an array:-");
		int size=sc.nextInt();
		int []arr=new int[size];
		int max=arr[0];
		
		for(int i=0;i<arr.length;i++){
			System.out.println("enter an element:-");
			arr[i]=sc.nextInt();
		}
		int min=arr[0];
		for(int i=0;i<arr.length;i++){
			
			if(arr[i]>max){
				max=arr[i];
			}
		}
		 for(int i=0;i<arr.length;i++){
			if(min>arr[i]){
                                min=arr[i];
                                System.out.println("min"+min);
                        }}
		System.out.println(max+" "+min);
		int diff=max-min;
		System.out.println("differenece:-"+diff);
	}
}
