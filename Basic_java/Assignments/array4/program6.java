/*
 *Q6. WAP to count the vowels and consonants in the given array(Take input from the user)
Example:
Enter the size of the array:
6
Enter the elements of the array:
a
E
P
o
U
G
Output:
Count of vowels: 4

Count of consonants: 2
 */
import java.util.*;
class Program6{
         public static void main(String []args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter size of an array:-");
                int size=sc.nextInt();
                char []arr=new char[size];
                for(int i=0;i<arr.length;i++){
                        System.out.println("enter an element:-");
                        arr[i]=sc.next().charAt(0);
               }
	       int count=0,count1=0;
	       for(int i=0;i<arr.length;i++){
	       		if(arr[i]=='a'||arr[i]=='e'||arr[i]=='i'||arr[i]=='o'||arr[i]=='u'){
				count++;
			}else{
				count1++;
			}

	       }
		System.out.println("vowels:-"+count);
		System.out.println("consonents:-"+count1);

          }
}
