/*
 *Q4. WAP to check whether the user given number occurs more than 2 times or equals 2
times.
Example :

Input:
Enter the size of the array:
6
Enter the elements of the array:
56
65
78
56
90
56
Enter the number to check:
56

56 occurs more than 2 times in the array.
 */
import java.util.*;
class Program4{
         public static void main(String []args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter size of an array:-");
                int count=0;
		int size=sc.nextInt();
                int []arr=new int[size];
		System.out.println("enter number to check:-");
		int search=sc.nextInt();
                for(int i=0;i<arr.length;i++){
                        System.out.println("enter an element:-");
                        arr[i]=sc.nextInt();
                }
		for(int i=0;i<arr.length;i++){
			if(search==arr[i]){
				count++;
			}
		
		}
		if(count>=2){
			System.out.println("count is more than 2 or equal to 2");
		}else{
			System.out.println("count is less than 2");
		}

          }
}
