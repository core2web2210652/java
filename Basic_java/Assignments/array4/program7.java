/*
 * WAp to convert lowercase chracter to uppercase characyer
 */
import java.util.*;
class Program7{
	public static void main(String []args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter size of an array:-");
		int size=sc.nextInt();
		char arr[]=new char[size];
		for(int i=0;i<arr.length;i++){
			System.out.print(" enter element: ");
			arr[i]=sc.next().charAt(0);

		}
		for(int i=0;i<arr.length;i++){
			if(Character.isLowerCase(arr[i])){
				arr[i]=Character.toUpperCase(arr[i]);
			}
		
		}
		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
	}
}
