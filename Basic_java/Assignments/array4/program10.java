/*
 * WAP to print character in an array which comes before user given character
 */
import java.util.*;
class Program10{
        public static void main(String []args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter size of an array:-");
                int size=sc.nextInt();
		System.out.println("enter character key:-");
		char search=sc.next().charAt(0);
                char arr[]=new char[size];
                for(int i=0;i<arr.length;i++){
                        System.out.print(" enter element: ");
                        arr[i]=sc.next().charAt(0);

                }
		int index=0;
                for(int i=0;i<arr.length;i++){
                        if(search==arr[i]){
                                index=i;
                        }

                }
                for(int i=0;i<index;i++){
                        System.out.println(arr[i]);
                }
        }
}
