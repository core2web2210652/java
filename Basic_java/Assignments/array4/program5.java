/*
 *
 *Q5. WAP to reverse the array(take input from the user).
Example:
Input:
Enter the size of the array:
6
Enter the elements of the array:
1
2
3
4
5
6
Output:
Reversed array:
6
5
4
3
2
1
 */
import java.util.*;
class Program5{
         public static void main(String []args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter size of an array:-");
                int size=sc.nextInt();
                int []arr=new int[size];
		int temp=0;
                for(int i=0;i<arr.length;i++){
                        System.out.println("enter an element:-");
                        arr[i]=sc.nextInt();
               }
	       for(int i=0;i<arr.length/2;i++){
	       		temp=arr[i];
			arr[i]=arr[size-i-1];
			arr[size-i-1]=temp;
	       }
	       for(int i=0;i<arr.length;i++){
	       		System.out.println(arr[i]);
	       }

          }
}
