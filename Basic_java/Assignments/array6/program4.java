/*Q4. Y ou have to take two different 1d arrays of the same size and print the
common elements from the arrays.
arr1
45 67 97 87 90 80
arr2
15 97 67 80 90 10
Output:
Common elements in the given arrays are: 67, 97, 90, 80

 */
import java.util.*;
class Program4{
        public static void main(String[]args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter size:");
                int size=sc.nextInt();
                int arr1[]=new int[size];
                int arr2[]=new int[size];
		System.out.println("array1:-");
                for(int i=0;i<arr1.length;i++){
                        System.out.println("enter an element:-");
                        arr1[i]=sc.nextInt();

                 }
		System.out.println("array2:-");
		 for(int i=0;i<arr2.length;i++){                                                                                                                                     System.out.println("enter an element:-");
                        arr2[i]=sc.nextInt();

                 }
		 int temp[]=new int[size];
		 int j=0;
		 Arrays.sort(arr1);
                for(int i=0;i<arr1.length-1;i++){
			if(arr1[i]!=arr1[i+1]){
				temp[j++]=arr1[i];
			}	                                                                                          
                	
		}	
		temp[j]=arr1[size-1];
		System.out.println("Common elements in the given arrays are:");
		for(int i=0;i<=j;i++){
			for(int k=0;k<arr2.length;k++){
				if(temp[i]==arr2[k]){
					System.out.print(temp[i]+", ");
				}
			}
		
		}
	}
		
 }
        

