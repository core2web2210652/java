/*
 * 8.write a program to print product of odd digits 
 */
import java.util.*;
class Program8{
        public static void main(String []args){
                int rem;
		int product=1;
                Scanner sc=new Scanner(System.in);
                System.out.println("enter number:-");
                int num=sc.nextInt();
                while(num!=0){
                        rem=num%10;
                        if(rem%2==1){
                                product*=rem;
				//System.out.println(rem);

                        }
                        num/=10;
                }
		System.out.println(product);
        }
}
