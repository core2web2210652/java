/*
 * 6.write a program to print product of digits in given number.
 *
 */
import java.util.*;
class Program6{
        public static void main(String []args){
                int rem;
		int product=1;
                Scanner sc=new Scanner(System.in);
                System.out.println("enter number:-");
                int num=sc.nextInt();
                while(num!=0){
                        rem=num%10;
                        product*=rem;
                        num/=10;
                
		}
		System.out.println(product);
        }
}
