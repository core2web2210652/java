/*
 * 5.write a program to print cubes of even digits of given number.
 *
 */
import java.util.*;
class Program5{
        public static void main(String []args){
                int rem;
                Scanner sc=new Scanner(System.in);
                System.out.println("enter number:-");
                int num=sc.nextInt();
                while(num!=0){
                        rem=num%10;
                        if(rem%2==0){
                                System.out.println(rem*rem*rem);

                        }
                        num/=10;
                }
        }
}
