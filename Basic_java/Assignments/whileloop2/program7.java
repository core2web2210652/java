/*
 * 7.write a program to print sum of even digits 
 *
 */
import java.util.*;
class Program7{
        public static void main(String []args){
                int rem;
		int sum=0;
                Scanner sc=new Scanner(System.in);
                System.out.println("enter number:-");
                int num=sc.nextInt();
                while(num!=0){
                        rem=num%10;
                        if(rem%2==0){
				sum+=rem;
                                //System.out.println(rem);

                        }
                        num/=10;
                }
		System.out.println(sum);
        }
}
