/*
 * 3.write a program to print digits grom given number which is divisible by 2 or 3
 *
 */
import java.util.*;
class Program3{
        public static void main(String []args){
                int rem;
                Scanner sc=new Scanner(System.in);
                System.out.println("enter number:-");
                int num=sc.nextInt();
                while(num!=0){
                        rem=num%10;
                        if(rem%2==0||rem%3==0){
                                System.out.println(rem);

                        }
                        num/=10;
                }
        }
}
