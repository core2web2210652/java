/*
 * 4.write a program to print square of odd digits of given number.
 *
 */
import java.util.*;
class Program4{
        public static void main(String []args){
                int rem;
                Scanner sc=new Scanner(System.in);
                System.out.println("enter number:-");
                int num=sc.nextInt();
                while(num!=0){
                        rem=num%10;
                        if(rem%2==1){
                                System.out.println(rem*rem);

                        }
                        num/=10;
                }
        }
}
