/*
 * 9.write a program to calculate sum of squares of odd digits in given number *
 */
import java.util.*;
class Program9{
        public static void main(String []args){
                int rem;
                Scanner sc=new Scanner(System.in);
                System.out.println("enter number:-");
                int num=sc.nextInt();
                while(num!=0){
                        rem=num%10;
                        if(rem%2==0){
                                System.out.println(rem);

                        }
                        num/=10;
                }
        }
}
