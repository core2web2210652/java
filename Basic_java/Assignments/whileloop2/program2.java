/*
 * 2.write a program to print digits grom given number which is not divisible by 3
 *
 */
import java.util.*;
class Program2{
        public static void main(String []args){
                int rem;
                Scanner sc=new Scanner(System.in);
                System.out.println("enter number:-");
                int num=sc.nextInt();
                while(num!=0){
                        rem=num%10;
                        if(rem%3!=0){
                                System.out.println(rem);

                        }
                        num/=10;
                }
        }
}
