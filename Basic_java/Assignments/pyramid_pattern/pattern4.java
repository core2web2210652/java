/*
 * 5.WAP in notebook & Dry run first then type
Take number of rows from user :
row=3

	1
      1 2 1
    1 2 3 2 1
row=4

1
1 2 1
1 2 3 2 1
1 2 3 4 3 2 1
*/
import java.util.*;
class Pattern4{
	public static void main (String []args)
	
	{	
		Scanner sc=new Scanner(System.in);
		System.out.println("enter number of rows");
		int rows=sc.nextInt();
		for(int i=0;i<rows;i++){
			int num=1;
			for(int j=0;j<rows-i;j++){
				System.out.print("  ");
			}
			for(int j=0;j<=i;j++){
				System.out.print(" "+num++);
			
			}
			num--;
			for(int j=0;j<i;j++){
				
				System.out.print (" "+--num);
			}
			System.out.println("");			
		}
	}
}
