/*2.WAP in notebook & Dry run first then type
Take number of rows from user :
row=3

    1
  2 3 4
5 6 7 8 9
row=4

      1
    2 3 4
  5 6 7 8 9
10 11 12 13 14 15 16
*/
import java.util.*;
class Pattern3{
	public static void main(String[]arg){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter no of rows:-");
		int rows=sc.nextInt();
		int a=1;
		for(int i=0;i<rows;i++){
			for(int j=rows-1;j>i;j--){
				System.out.print("  ");

			}
			for(int j=0;j<=i;j++){
				System.out.print( " "+a++);
			}
			for(int j=0;j<i;j++){
				System.out.print( " "+a++);
			}
			System.out.println("");
			
		}
	}
}
