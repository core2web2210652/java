/*
 * 6.WAP in notebook & Dry run first then type
Take number of rows from user :

row=3

	3
      3 2 3
    3 2 1 2 3
row=4

4
4 3 4
4 3 2 3 4
4 3 2 1 2 3 4
*/
import java.util.*;
class Pattern5{
	public static void main(String []args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter no of rows");
		int rows=sc.nextInt();
		for(int i=0;i<rows;i++){
			int num=rows;
			for(int j=0;j<rows-i;j++){
				System.out.print("  ");
			}
			for(int j=0;j<=i;j++){
				 System.out.print(" "+num--);
			}
			num+=1;
			for(int j=0;j<i;j++)
			{
				 System.out.print(" "+ (++num));
			
			}
			 System.out.println("");
		}
	}
}
