//pattren code2:-
/*1.WAP in notebook & Dry run first then type
Take number of rows from user :
row=3

    1
  1 1 1
1 1 1 1 1
row=4

      1
    1 1 1
  1 1 1 1 1
1 1 1 1 1 1 1
*/
import java.util.*;
class Pattern1{
        public static void main(String []args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter number of rows");
                int rows=sc.nextInt();
                for(int i=0;i<rows;i++){
                        for(int j=rows-1;j>i;j--){
                                System.out.print("  ");
                        }
                        for(int j=0;j<=i;j++){
                                System.out.print(" 1");
                        }
                        for(int j=0;j<i;j++){
                                System.out.print(" 1");
                        }
                        System.out.println("");
                }
        }
}
