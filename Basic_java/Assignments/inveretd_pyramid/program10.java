/*
 * 10.WAP in notebook & Dry run first then type

Take number of rows from user :
rows:3

3 2 1 2 3
2 1 2
1

rows:4
4 3 2 1 2 3 4
3 2 1 2 3
2 1 2
1
 */
import java.util.Scanner;
class Program10{
        public static void main(String []args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter no of rows:-");
                int rows=sc.nextInt();
                int val1=rows;
                for(int i=0;i<rows;i++){
                        int val=val1;
                        for(int j=0;j<i;j++){
                                System.out.print("  ");
                        }

                        for(int j=0;j<rows-i;j++){
                                System.out.print(" "+val--);
                        }
                        val+=2;
                         for(int j=0;j<(rows-i-1);j++){
                                System.out.print(" "+val++);
                        }
                        for(int j=0;j<i;j++){
                                System.out.print(" ");
                        }
                        val1--;
                        System.out.println("");
                }

        }

}
