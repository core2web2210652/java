/*
 * 9.WAP in notebook & Dry run first then type
Take number of rows from user :
row=3

1 0 1 0 1
1 0 1
1

row = 4
1 0 1 0 1 0 1
1 0 1 0 1
1 0 1
1
 */
import java.util.Scanner;
class Program9{
        public static void main(String []args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter no of rows:-");
                int rows=sc.nextInt();
                
                for(int i=0;i<rows;i++){
                        int val=1;
                        for(int j=0;j<i;j++){
                                System.out.print("  ");
                        }

                        for(int j=0;j<rows-i;j++){
				if(j%2==0){
					 System.out.print(" "+val);
				}
				else{
					 System.out.print(" 0");
				}
                        }
                       
                         for(int j=0;j<(rows-i-1);j++){
                                if(j%2==0){
					 System.out.print(" 1");
				}else{
					 System.out.print(" 0");
				}
                        }
                        for(int j=0;j<i;j++){
                                System.out.print(" ");
                        }
                        
                        System.out.println("");
                }

        }

}
