/*
 * WAP to check whether number is happy number or not.
 */
import java.util.Scanner;
class Happy{                                          
	public static void main(String []args){
                Scanner sc=new Scanner(System.in);
                System.out.println("enter a number");
                 int num=sc.nextInt();
		int rem=0,quotient=num,sum=0;
		while(quotient!=0){
			rem=quotient%10;
			sum+=(int)Math.pow(rem,2);
			quotient/=10;
		}
		if(sum==1){
			System.out.println(num+" is happy number");
		}else{
			 System.out.println(num+" is not happy number");
		}		
      }                                                                      
} 
