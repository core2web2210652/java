/*
 * WAP to check whether given number is deficient or not.
 */
import java.util.*;
class Deficient{
	public static void main(String []args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter a number");
		int num=sc.nextInt();
		int sum=0;
		for(int i=1;i<num;i++){
			if(num%i==0){
				sum+=i;
			}
		}
		if(sum<num){
			System.out.println("number is deficient");
		}else{
			 System.out.println("number is not deficient");
		}
	}
}
