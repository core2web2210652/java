/*
 * WAP to check whether given number is strong number or not.
 */
import java.util.*;
class Strong{
	public static void main(String []args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter a number");
		int num=sc.nextInt();
		int quotient=num,rem=0,sum=0;
		while(quotient!=0){
			rem=quotient%10;
			int product=1;
			for(int i=1;i<=rem;i++){
				product*=i;
			}
			sum+=product;
			quotient/=10;
		}
		if(sum==num){
			System.out.println(num+" is strong number");
		}else{
			System.out.println(num+" is not strong number");
		}
	}

}
