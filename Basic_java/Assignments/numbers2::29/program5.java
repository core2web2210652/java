/*
 * WAP to check whether given number is automorphic or not.
 */
import java.util.*;
class Automorphic{
	public static void main(String []args){
		Scanner sc=new Scanner(System.in);
		System.out.println("enter a number");
		int num=sc.nextInt();
		int quotient1=num;
		int sum1=0,rem1=0,sum2=0,rem2=0,count1=0,count2=0;
		int quotient2=(int)(Math.pow(num,2));
		
		while(quotient1!=0){
			rem1=quotient1%10;
			
			count1++;
			quotient1/=10;	
		}
		 while(quotient2!=0){
                        rem1=quotient2%10;

                        count2++;
                        quotient2/=10;
                }
		int arr1[]=new int[count1];
		int arr2[]=new int[count2];
		quotient1=num;
		quotient2=(int)Math.pow(num,2);
		count1=0;
		count2=0; 
		while(quotient1!=0){
                        rem1=quotient1%10;
			arr1[count1++]=rem1;
                        
                        quotient1/=10;
                }
                 while(quotient2!=0){
                        rem2=quotient2%10;
			arr2[count2++]=rem2;
                        
                        quotient2/=10;
                }
		int count=0;
		for(int i=0;i<arr1.length;i++){
		
			if(arr1[i]==arr2[i]){
				count++;
			}
				
			
		}
		if(count==arr1.length){
			System.out.println("number is automorphic");
		}else{
			System.out.println("number is not automorphic");
		}
	}

}
